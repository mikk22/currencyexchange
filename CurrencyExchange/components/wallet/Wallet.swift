//
//  Wallet.swift
//  CurrencyExchange
//
//  Created by Mikhail Koltsov on 09.10.17.
//  Copyright © 2017 Mikhail Koltsov. All rights reserved.
//

import Foundation

class Wallet {
    var currencyRates: CurrencyRates?

    var amount: Decimal? = 1

    var currency: String = "EUR" {
        willSet {
            amount = convertAmount(to: newValue)
        }

        didSet {
            currencyRates?.convert(to: currency)
            save(currency: currency)
        }
    }

    init() {
        if let currency = loadCurrency() {
            self.currency = currency
        }
    }

    func convertAmount(to currency: String) -> Decimal? {
        if self.currency == currency {
            return amount
        }

        guard let rates = currencyRates?.rates,
            let amount = self.amount else {
            return nil
        }

        let rate = Decimal(rates[currency] as? Double  ?? 1.0)
        return amount * rate
    }
}

extension Wallet {
    private var walletCurrencyKey: String {
        return "wallet.currency.key"
    }

    fileprivate func loadCurrency() -> String? {
        let userDefauts = UserDefaults.standard
        return userDefauts.string(forKey: walletCurrencyKey)
    }
    
    fileprivate func save(currency: String) {
        let userDefauts = UserDefaults.standard
        userDefauts.set(currency, forKey: walletCurrencyKey)
    }
}

extension Wallet {
    var currencyItem: CurrencyItem {
        return CurrencyItem(currency: currency)
    }
}
