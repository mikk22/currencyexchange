//
//  CurrencyRatesFetcher.swift
//  CurrencyExchange
//
//  Created by Mikhail Koltsov on 07.10.17.
//  Copyright © 2017 Mikhail Koltsov. All rights reserved.
//

import Alamofire
import Foundation
import ObjectMapper

protocol CurrencyRatesFetcherDelegate: class {
    func fetcher(_ fetcher: CurrencyRatesFetcher,
                 didLoad currencyRates: CurrencyRates?)
}

//https://revolut.duckdns.org/latest?base=EUR

class CurrencyRatesFetcher {
    private let baseUrl = "https://revolut.duckdns.org"
    private var dataRequest: DataRequest?
    private var timer: Timer?

    var currency: String {
        didSet {
            invalidateCurrentLoadProcess()
            loadLatestRate()
        }
    }

    weak var delegate: CurrencyRatesFetcherDelegate?

    init(currency: String) {
        self.currency = currency
    }

    deinit {
        invalidateCurrentLoadProcess()
    }

    func fetchLatestRate(with currency: String, completion: ((CurrencyRates?)->())? ) {
        let url = URL(string: baseUrl + "/latest")
        let parameters = ["base": currency]

      let dataRequest = AF.request(url!, parameters: parameters)
        dataRequest.responseJSON { response in
            if let json = try? response.result.get() {
                let result = Mapper<CurrencyRates>().map(JSONObject: json)
                completion?(result)
                return
            }

            completion?(nil)
        }
    }

    func startUpdating() {
        scheduleUpdateLatestRateTimer()
    }

    func finishUpdating() {
        invalidateCurrentLoadProcess()
    }

    private func scheduleUpdateLatestRateTimer() {
        invalidateCurrentLoadProcess()
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false, block: { [weak self] (timer) in
            self?.loadLatestRate()
        })
    }

    private func invalidateCurrentLoadProcess() {
        timer?.invalidate()
        dataRequest?.cancel()
    }

    private func loadLatestRate() {
        fetchLatestRate(with: currency) { [weak self] currencyRates in
            self?.dataRequest = nil
            self?.delegate?.fetcher(self!, didLoad: currencyRates)
            self?.scheduleUpdateLatestRateTimer()
        }
    }
}
