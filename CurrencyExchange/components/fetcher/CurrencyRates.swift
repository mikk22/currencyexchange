//
//  CurrencyRates.swift
//  CurrencyExchange
//
//  Created by Mikhail Koltsov on 07.10.17.
//  Copyright © 2017 Mikhail Koltsov. All rights reserved.
//

import Foundation
import ObjectMapper

class CurrencyRates: Mappable {
    private(set) var base: String?
    private(set) var rates: Dictionary<String, Any> = [:]

    required init?(map: Map) {
        mapping(map: map)
    }

    func mapping(map: Map) {
        base <- map["base"]
        rates <- map["rates"]
    }

    func convert(to currency: String) {
        guard rates.keys.count > 0,
            let base = self.base else {
                return
        }

        let rate = rates[currency] as! Double
        rates.removeValue(forKey: currency)
        rates[base] = 1.0

        rates.forEach { (key, value) in
            rates[key] = value as! Double / rate
        }

        self.base = currency
    }
}
