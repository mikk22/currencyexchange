//
//  ExchangeViewController.swift
//  CurrencyExchange
//
//  Created by Mikhail Koltsov on 07.10.17.
//  Copyright © 2017 Mikhail Koltsov. All rights reserved.
//

import UIKit

class ExchangeViewController: UIViewController,
                              ExchangeModelDelegate,
                              KeyboardAppearanceInfoControllerDelegate,
                              UICollectionViewDataSource,
                              UICollectionViewDelegate,
                              UICollectionViewDelegateFlowLayout,
                              CurrencyItemCellDelegate {
    private let keyboardAppearanceInfoController = KeyboardAppearanceInfoController()
    private let model = ExchangeModel()
    
    private let currencyItemCellIdentifier = "currencyItemCellIdentifier"
    
    @IBOutlet private weak var collectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        keyboardAppearanceInfoController.delegate = self
        model.delegate = self
        setupCollectionView()
    }
    
    private func setupCollectionView() {
        collectionView.register(UINib(nibName: "CurrencyItemCell", bundle: nil),
                                forCellWithReuseIdentifier: currencyItemCellIdentifier)
        collectionView.dataSource = self
        collectionView.delegate = self
        let layout = UICollectionViewFlowLayout()
        collectionView.collectionViewLayout = layout
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        model.start()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        model.finish()
    }
    
    // MARK: - KeyboardAppearanceInfoControllerDelegate
    
    func keyboardAppearanceInfoControllerWillShow(_ controller: KeyboardAppearanceInfoController,
                                                  info: KeyboardAppearanceInfo) {
        let keyboardHeight = info.keyboardFrameIn(view: view).height
        UIView.animate(withDuration: info.animationDuration,
                       delay: 0.0,
                       options: info.animationOptions,
                       animations: {
                        var contentInset = self.collectionView.contentInset
                        contentInset.bottom = keyboardHeight
                        self.collectionView.contentInset = contentInset
        },
                       completion: nil)
    }

    func keyboardAppearanceInfoControllerWillHide(_ controller: KeyboardAppearanceInfoController,
                                                  info: KeyboardAppearanceInfo) {
        UIView.animate(withDuration: info.animationDuration,
                       delay: 0.0,
                       options: info.animationOptions,
                       animations: {
                        var contentInset = self.collectionView.contentInset
                        contentInset.bottom = 0.0
                        self.collectionView.contentInset = contentInset
        },
                       completion: nil)
    }

    // MARK: - UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return model.itemsCount()
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: currencyItemCellIdentifier,
                                                      for: indexPath) as! CurrencyItemCell
        update(cell, at: indexPath)
        cell.delegate = self
        return cell
    }

    private func update(_ cell: CurrencyItemCell, at indexPath: IndexPath) {
        guard let data = model.itemData(at: indexPath) else {
            return
        }
        
        cell.update(data)
    }

    // MARK: - UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView,
                        willDisplay cell: UICollectionViewCell,
                        forItemAt indexPath: IndexPath) {
        let castedCell = cell as! CurrencyItemCell
        update(castedCell , at: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! CurrencyItemCell
        cell.setBecomeActive()
    }

    // MARK: - UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width, height: 70.0)
    }

    // MARK: - ExchangeModelDelegate

    func modelDidLoad(_ model: ExchangeModel) {
        if isViewLoaded == false {
            return
        }

        collectionView.reloadData()
    }
    
    func modelDidUpdate(_ model: ExchangeModel) {
        if isViewLoaded == false {
            return
        }

        updateVisibleItems()
    }
    
    func model(_ model: ExchangeModel, didMoveItemAt indexPath: IndexPath, to newIndexPath: IndexPath) {
        if isViewLoaded == false {
            return
        }

        collectionView.performBatchUpdates({
            collectionView.moveItem(at: indexPath, to: newIndexPath)
        }) { (finished) in
            self.collectionView.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: true)
        }
    }

    private func updateVisibleItems() {
        let indices = collectionView.indexPathsForVisibleItems
        for indexPath in indices {
            if indexPath == IndexPath(item: 0, section: 0) {
                continue
            }

            let cell = collectionView.cellForItem(at: indexPath) as! CurrencyItemCell
            update(cell, at: indexPath)
        }
    }

    // MARK: - CurrencyItemCellDelegate

    func currencyItemCellDidBeginEditing(_ cell: CurrencyItemCell) {
        guard let indexPath = collectionView.indexPath(for: cell) else {
            return
        }

        model.setWalletCurrencyItem(at: indexPath)
    }

    func currencyItemCell(_ cell: CurrencyItemCell, didChange amount: Decimal?) {
        model.set(amount: amount)
        updateVisibleItems()
    }
}
