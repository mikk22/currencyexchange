//
//  ExchangeModel.swift
//  CurrencyExchange
//
//  Created by Mikhail Koltsov on 07.10.17.
//  Copyright © 2017 Mikhail Koltsov. All rights reserved.
//

import Foundation

protocol ExchangeModelDelegate: class {
    func modelDidLoad(_ model: ExchangeModel)
    func modelDidUpdate(_ model: ExchangeModel)
    func model(_ model: ExchangeModel, didMoveItemAt indexPath: IndexPath, to newIndexPath: IndexPath)
}

class ExchangeModel: CurrencyRatesFetcherDelegate {
    private let wallet = Wallet()
    private let fetcher: CurrencyRatesFetcher
    private let formatter = CurrencyFormatter.createFormatter()

    private var items: [CurrencyItem] = []
    weak var delegate: ExchangeModelDelegate?

    init() {
        fetcher = CurrencyRatesFetcher(currency: wallet.currency)
        fetcher.delegate = self
    }

    func set(amount: Decimal?) {
        wallet.amount = amount
    }

    func start() {
        fetcher.startUpdating()
    }
    
    func finish() {
        fetcher.finishUpdating()
    }

    func itemsCount() -> Int {
        return items.count
    }

    func itemData(at indexPath: IndexPath) -> CurrencyItemCellData? {
        let item = items[indexPath.row]
        let currency = item.currency

        var formattedAmount: String?
        if let amount = wallet.convertAmount(to: currency) {
            formattedAmount = formatter.string(from: amount as NSNumber)
        }

        return CurrencyItemCellData(currency: currency, amount: formattedAmount)
    }

    func setWalletCurrencyItem(at indexPath: IndexPath) {
        let newIndex = 0
        if indexPath.item == newIndex {
            return
        }

        let index = indexPath.row
        let item = items[index]
        items.remove(at: index)
        items.insert(item, at: newIndex)

        set(currency: item.currency)
        delegate?.model(self, didMoveItemAt: indexPath, to: IndexPath(item: newIndex, section: 0))
    }

    private func set(currency: String) {
        wallet.currency = currency
        fetcher.currency = currency
    }

    //MARK: - CurrencyRatesFetcherDelegate

    func fetcher(_ fetcher: CurrencyRatesFetcher,
                 didLoad currencyRates: CurrencyRates?) {
        wallet.currencyRates = currencyRates
        processItems()
    }

    private func processItems() {
        if items.count == 0 {
            items = createItems()
            delegate?.modelDidLoad(self)
        } else {
            delegate?.modelDidUpdate(self)
        }
    }

    private func createItems() -> [CurrencyItem] {
        var items = [wallet.currencyItem]
        guard let rates = wallet.currencyRates?.rates else {
            return items
        }

        let mapped = rates.map { (key: String, value: Any) -> CurrencyItem in
            return CurrencyItem(currency: key)
        }

        items.append(contentsOf: mapped)
        return items
    }
}
