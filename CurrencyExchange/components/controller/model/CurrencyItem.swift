//
//  CurrencyItem.swift
//  CurrencyExchange
//
//  Created by Mikhail Koltsov on 08.10.17.
//  Copyright © 2017 Mikhail Koltsov. All rights reserved.
//

import Foundation

struct CurrencyItem : CustomStringConvertible, Hashable, Equatable {
    let currency: String

    init(currency: String) {
        self.currency = currency
    }

    // MARK: - Hashable

    func hash(into hasher: inout Hasher) {
        hasher.combine(currency.hashValue)
    }

    // MARK: - Equatable

    static func == (lhs: CurrencyItem, rhs: CurrencyItem) -> Bool {
        return lhs.currency == rhs.currency
    }
    
    // MARK: - CustomStringConvertible

    var description: String {
        return currency
    }
}
