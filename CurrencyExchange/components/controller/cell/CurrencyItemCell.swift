//
//  CurrencyItemCell.swift
//  CurrencyExchange
//
//  Created by Mikhail Koltsov on 07.10.17.
//  Copyright © 2017 Mikhail Koltsov. All rights reserved.
//

import UIKit

protocol CurrencyItemCellDelegate: class {
    func currencyItemCellDidBeginEditing(_ cell: CurrencyItemCell)
    func currencyItemCell(_ cell: CurrencyItemCell, didChange amount: Decimal?)
}

class CurrencyItemCell: UICollectionViewCell,
                        UITextFieldDelegate {
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var currencyLabel: UILabel!
    @IBOutlet private weak var textField: UnderlinedTextField!

    weak var delegate: CurrencyItemCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        imageView.layer.cornerRadius = 25.0
        textField.delegate = self
        textField.addTarget(self, action: #selector(editingChanged), for: .editingChanged)
        
    }

    @objc private func editingChanged() {
        var value: Decimal?
        if let string = textField.text {
            value = Decimal(string: string)
        }

        delegate?.currencyItemCell(self, didChange: value)
    }

    func setBecomeActive() {
        textField.becomeFirstResponder()
    }

    func update(_ data: CurrencyItemCellData) {
        currencyLabel.text = data.currency
        textField.text = data.amount
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        currencyLabel.text = nil
        textField.text = nil
    }

    //MARK: - UITextFieldDelegate

    func textFieldDidBeginEditing(_ textField: UITextField) {
        delegate?.currencyItemCellDidBeginEditing(self)
    }

    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool  {
        let currentCharacterCount = textField.text?.count ?? 0
        if (range.length + range.location > currentCharacterCount){
            return false
        }
        let newLength = currentCharacterCount + string.count - range.length
        return newLength <= 18
    }
}
