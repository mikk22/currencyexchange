//
//  CurrencyItemCellData.swift
//  CurrencyExchange
//
//  Created by Mikhail Koltsov on 07.10.17.
//  Copyright © 2017 Mikhail Koltsov. All rights reserved.
//

import Foundation

struct CurrencyItemCellData {
    let currency: String
    let amount: String?
}
