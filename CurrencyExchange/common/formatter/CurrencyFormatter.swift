//
//  CurrencyFormatter.swift
//  CurrencyExchange
//
//  Created by Mikhail Koltsov on 08.10.17.
//  Copyright © 2017 Mikhail Koltsov. All rights reserved.
//

import Foundation

class CurrencyFormatter {
    static func createFormatter() -> NumberFormatter {
        let formatter = NumberFormatter()
        formatter.usesGroupingSeparator = false
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 2
        formatter.minimumIntegerDigits = 1
        formatter.roundingMode = .halfUp
        return formatter
    }
}
