//
//  UnderlinedTextField.swift
//  CurrencyExchange
//
//  Created by Mikhail Koltsov on 08.10.17.
//  Copyright © 2017 Mikhail Koltsov. All rights reserved.
//

import UIKit

class UnderlinedTextField: UITextField {

    private var underlineLayer: CALayer!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    private func commonInit() {
        addTarget(self, action: #selector(editingChanged), for: .editingChanged)
        addTarget(self, action: #selector(editingDidBegin), for: .editingDidBegin)
        addTarget(self, action: #selector(editingDidEnd), for: .editingDidEnd)
        
        underlineLayer = CALayer()
        underlineLayer.backgroundColor = UIColor.lightGray.cgColor
        layer.addSublayer(underlineLayer)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        let textSize = sizeThatFits(bounds.size)
        let height: CGFloat = isFirstResponder ? 2.0 : 1.0
        let origin = CGPoint(x: bounds.width - textSize.width, y: bounds.height - height)
        let size = CGSize(width: textSize.width, height: height)

        CATransaction.setDisableActions(true)
        underlineLayer.frame = CGRect(origin: origin, size: size)
        CATransaction.commit()
    }

    @objc private func editingChanged() {
        setNeedsLayout()
    }

    @objc private func editingDidBegin() {
        underlineLayer.backgroundColor = UIColor.blue.cgColor
    }

    @objc private func editingDidEnd() {
        underlineLayer.backgroundColor = UIColor.lightGray.cgColor
    }
}
