//
//  KeyboardAppearanceInfo.swift
//  CurrencyExchange
//
//  Created by Mikhail Koltsov on 07.10.17.
//  Copyright © 2017 Mikhail Koltsov. All rights reserved.
//

import UIKit
import CoreGraphics

class KeyboardAppearanceInfo: NSObject {
    let animationDuration: TimeInterval
    let animationOptions: UIView.AnimationOptions
    let frameBegin: CGRect
    let keyboardFrame: CGRect
    
    init(userInfo: Dictionary<AnyHashable, Any>) {
        animationDuration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! TimeInterval
        frameBegin = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let rawAnimationCurve = (userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as! NSNumber).uint32Value << 16
        animationOptions = UIView.AnimationOptions.init(rawValue: UInt(rawAnimationCurve))

        super.init()
    }
    
    func keyboardFrameIn(view: UIView) -> CGRect {
        let frame = keyboardFrame
        let applicationWindow = UIApplication.shared.windows[0]
        if view.window != applicationWindow {
            return frame
        }
        
        return applicationWindow.convert(frame, to: view)
    }
}
