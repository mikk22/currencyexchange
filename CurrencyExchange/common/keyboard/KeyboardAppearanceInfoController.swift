//
//  KeyboardAppearanceInfoController.swift
//  CurrencyExchange
//
//  Created by Mikhail Koltsov on 07.10.17.
//  Copyright © 2017 Mikhail Koltsov. All rights reserved.
//

import UIKit

import Foundation

@objc protocol KeyboardAppearanceInfoControllerDelegate: class {
    func keyboardAppearanceInfoControllerWillShow(_ controller: KeyboardAppearanceInfoController,
                                                  info: KeyboardAppearanceInfo)
    @objc optional func keyboardAppearanceInfoControllerDidShow(_ controller: KeyboardAppearanceInfoController)
    func keyboardAppearanceInfoControllerWillHide(_ controller: KeyboardAppearanceInfoController,
                                                  info: KeyboardAppearanceInfo)
    @objc optional func keyboardAppearanceInfoControllerDidHide(_ controller: KeyboardAppearanceInfoController)
}

class KeyboardAppearanceInfoController: NSObject {
    weak var delegate: KeyboardAppearanceInfoControllerDelegate?
    
    deinit {
        unsubscribeFromKeyboardNotifications()
    }
    
    override init() {
        super.init()
        subscribeForKeyboardNotifications()
    }
    
    private func subscribeForKeyboardNotifications() {
        let center = NotificationCenter.default
        center.addObserver(forName: UIResponder.keyboardWillShowNotification, object: nil, queue: nil, using: keyboardWillShow)
        center.addObserver(forName: UIResponder.keyboardDidShowNotification, object: nil, queue: nil, using: keyboardDidShow)
        center.addObserver(forName: UIResponder.keyboardWillHideNotification, object: nil, queue: nil, using: keyboardWillHide)
        center.addObserver(forName: UIResponder.keyboardDidHideNotification, object: nil, queue: nil, using: keyboardDidHide)
    }
    
    private func unsubscribeFromKeyboardNotifications() {
        let center = NotificationCenter.default
        center.removeObserver(self)
    }
    
    private func keyboardWillShow(notification: Notification) {
        if let userInfo = notification.userInfo {
            let info = KeyboardAppearanceInfo(userInfo: userInfo)
            delegate?.keyboardAppearanceInfoControllerWillShow(self, info: info)
        }
    }
    
    private func keyboardDidShow(notification: Notification) {
        delegate?.keyboardAppearanceInfoControllerDidShow?(self)
    }
    
    private func keyboardWillHide(notification: Notification) {
        if let userInfo = notification.userInfo {
            let info = KeyboardAppearanceInfo(userInfo: userInfo)
            delegate?.keyboardAppearanceInfoControllerWillHide(self, info: info)
        }
    }
    
    private func keyboardDidHide(notification: Notification) {
        delegate?.keyboardAppearanceInfoControllerDidHide?(self)
    }
}

